run: dependencies up show_host

up:
	@docker-compose up -d --build

dependencies:
	@composer install --working-dir=src

tests:
	@./src/vendor/bin/phpunit --configuration src/phpunit.xml

show_host:
	@docker-compose port nginx 80 | sed -e 's/0\.0\.0\.0/http\:\/\/localhost/g'

down:
	@docker-compose down