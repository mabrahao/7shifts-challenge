#### How to execute:

```
$ git clone https://gitlab.com/mabrahao/7shifts-challenge
$ cd 7shifts-challenge
$ make run
```

A host will be displayed in the terminal. Copy it and add the sufix `/api/process`

Ex: `http://localhost:32787/api/process`
