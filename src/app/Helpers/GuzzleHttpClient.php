<?php

namespace App\Helpers;

use GuzzleHttp\Client;

final class GuzzleHttpClient implements HttpClient
{
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }


    public function getBodyOf(string $url): string
    {
        $response = $this->client->get($url);
        return $response->getBody()->getContents();
    }
}