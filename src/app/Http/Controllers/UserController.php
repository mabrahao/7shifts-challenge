<?php

namespace App\Http\Controllers;

use App\Services\CalculatorService;

class UserController extends Controller
{
    public function getByLocation(CalculatorService $calculatorService) {
        return response()->json($calculatorService->getUsersHoursByLocation(), 200);
    }
}
