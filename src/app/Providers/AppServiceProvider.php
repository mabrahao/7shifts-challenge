<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\HttpClient','App\Helpers\GuzzleHttpClient');

        $this->app->bind('App\Repositories\HttpLocationRepository', function () {
            return new \App\Repositories\HttpLocationRepository(
                app('App\Helpers\HttpClient'),
                config('repositories.location.url')
            );
        });

        $this->app->bind('App\Repositories\HttpPunchRepository', function () {
            return new \App\Repositories\HttpPunchRepository(
                app('App\Helpers\HttpClient'),
                config('repositories.punch.url')
            );
        });

        $this->app->bind('App\Repositories\LocationRepository','App\Repositories\HttpLocationRepository');
        $this->app->bind('App\Repositories\PunchRepository','App\Repositories\HttpPunchRepository');
    }
}
