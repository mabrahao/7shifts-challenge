<?php

namespace App\Repositories;


use App\Helpers\HttpClient;

class HttpPunchRepository implements PunchRepository
{

    /**
     * @var HttpClient
     */
    private $httpClient;
    /**
     * @var string
     */
    private $url;

    public function __construct(HttpClient $httpClient, string $url)
    {
        $this->httpClient = $httpClient;
        $this->url = $url;
    }

    public function fetchAll(): array
    {
        return $this->getDictFromHttpClient();
    }

    private function getDictFromHttpClient()
    {
        $content = $this->httpClient->getBodyOf($this->url);
        return json_decode($content, true);
    }

}