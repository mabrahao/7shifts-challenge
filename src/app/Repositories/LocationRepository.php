<?php

namespace App\Repositories;

interface LocationRepository
{
    public function fetchAll(): array;
}