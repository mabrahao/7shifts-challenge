<?php

namespace App\Services;

use App\Repositories\LocationRepository;
use App\Repositories\PunchRepository;

final class CalculatorService
{
    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * @var PunchRepository
     */
    private $punchRepository;

    public function __construct(LocationRepository $locationRepository, PunchRepository $punchRepository)
    {
        $this->locationRepository = $locationRepository;
        $this->punchRepository = $punchRepository;
    }

    public function getUsersHoursByLocation(): array
    {
        $processedPunches = $this->getProcessedPunches();
        $response = [];
        foreach ($processedPunches["locations"] as $locationId => $punch) {
            $users = $punch["users"];
            $response["locations"][$locationId] = ["users" => []];

            foreach ($users as $userId => $user) {
                $weeks = $user["week"];
                foreach ($weeks as $weekNumber => $week) {
                    $workedMinutes = $week["workedMinutes"];
                    $weeklyOvertime = $week["weeklyOvertime"] ?? 0;
                    $dailyOvertime = $week["dailyOvertime"] ?? 0;

                    $formattedUser = [
                        $userId => [
                            "weeks" => [
                                $weekNumber => [
                                    "worked_hours" => $workedMinutes,
                                    "daily_overtime" => $dailyOvertime,
                                    "weekly_overtime" => $weeklyOvertime
                                ]
                            ]
                        ]
                    ];

                    $response["locations"][$locationId]["users"] += $formattedUser;
                }
            }
        }

        return $response;
    }

    private function getProcessedPunches(): array
    {
        $locations = $this->locationRepository->fetchAll();
        $punches = $this->punchRepository->fetchAll();

        $processedPunches = ["locations"];

        foreach ($punches as $punch) {
            if ($punch["clockedIn"] && $punch["clockedOut"]) {

                $checkedIn = strtotime($punch["clockedIn"]);
                $checkedOut = strtotime($punch["clockedOut"]);

                $userId = $punch["userId"];
                $locationId = $punch["locationId"];
                if ($location = $locations[$locationId]) {
                    $dailyOvertimeThreshold = $location["labourSettings"]["dailyOvertimeThreshold"];
                    $weeklyOvertimeThreshold = $location["labourSettings"]["weeklyOvertimeThreshold"];

                    $date = new \DateTime($punch["clockedIn"]);
                    $checkInDay = $date->format('Y-m-d');
                    $week = $date->format("W");

                    $processedPunches = $this->processPunch($processedPunches, $locationId, $userId, $checkInDay, $week, $checkedOut, $checkedIn, $dailyOvertimeThreshold, $weeklyOvertimeThreshold);
                }
            }
        }

        return $processedPunches;
    }

    /**
     * @param $processedPunches
     * @param $locationId
     * @param $userId
     * @param $checkInDay
     * @param $week
     * @param $checkedOut
     * @param $checkedIn
     * @param $dailyOvertimeThreshold
     * @param $weeklyOvertimeThreshold
     * @return mixed
     */
    private function processPunch($processedPunches, $locationId, $userId, $checkInDay, $week, $checkedOut, $checkedIn, $dailyOvertimeThreshold, $weeklyOvertimeThreshold)
    {
        $processedPunches["locations"][$locationId]["users"][$userId][$checkInDay]["week"] = $processedPunches["locations"][$locationId]["users"][$userId][$checkInDay]["week"] ?? $week;

        $processedPunches["locations"][$locationId]["users"][$userId][$checkInDay]["workedMinutes"] = $processedPunches["locations"][$locationId]["users"][$userId][$checkInDay]["workedMinutes"] ?? 0;
        $processedPunches["locations"][$locationId]["users"][$userId]["week"][$week]["workedMinutes"] = $processedPunches["locations"][$locationId]["users"][$userId]["week"][$week]["workedMinutes"] ?? 0;
        $workedMinutes = ($checkedOut - $checkedIn) / 60;
        $workedMinutesByDay = ($processedPunches["locations"][$locationId]["users"][$userId][$checkInDay]["workedMinutes"] += $workedMinutes);

        if ($workedMinutesByDay > $dailyOvertimeThreshold) {
            $processedPunches["locations"][$locationId]["users"][$userId]["week"][$week]["dailyOvertime"] = $workedMinutesByDay - $dailyOvertimeThreshold;
        }

        $workedMinutesByWeek = ($processedPunches["locations"][$locationId]["users"][$userId]["week"][$week]["workedMinutes"] += $workedMinutes);

        if ($workedMinutesByWeek > $weeklyOvertimeThreshold) {
            $processedPunches["locations"][$locationId]["users"][$userId]["week"][$week]["weeklyOvertime"] = $workedMinutesByWeek - $weeklyOvertimeThreshold;
        }
        return $processedPunches;
    }

}