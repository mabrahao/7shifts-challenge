<?php

return [
    'location' => [
        'url' => env('LOCATION_REPOSITORY_URL', 'https://shiftstestapi.firebaseio.com/locations.json'),
    ],
    'punch' => [
        'url' => env('PUNCH_REPOSITORY_URL', 'https://shiftstestapi.firebaseio.com/timePunches.json'),
    ],
];