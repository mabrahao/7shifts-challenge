<?php

use App\Repositories\LocationRepository;
use App\Repositories\PunchRepository;
use App\Services\CalculatorService;

class CalculatorServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testJsonParserWithTwoUsers()
    {
        // setup

        $locationRepository = $this->getMockedLocationRepositoryWith(
            storage_path("testing/json/locations.json")
        );


        $punchRepository = $this->getMockedPunchRepositoryWith(
            storage_path("testing/json/timePunches.json")
        );


        $calculatorService = new CalculatorService($locationRepository, $punchRepository);

        // execution
        $actual = $calculatorService->getUsersHoursByLocation();
        $actualLocationCount = count($actual["locations"]);
        $firstLocation = current($actual["locations"]);
        $actualUserCount = count($firstLocation["users"]);

        // assertions
        $this->assertEquals(1, $actualLocationCount);
        $this->assertEquals(2, $actualUserCount);
    }


    private function getMockedLocationRepositoryWith(string $locationJsonPath)
    {
        $content = file_get_contents($locationJsonPath);

        $allLocations = json_decode($content, true);

        $locationRepository = $this->createMock(LocationRepository::class);

        $locationRepository->expects($this->once())
            ->method('fetchAll')
            ->willReturn($allLocations);

        return $locationRepository;
    }

    private function getMockedPunchRepositoryWith($punchJsonPath)
    {
        $content = file_get_contents($punchJsonPath);

        $allPunches = json_decode($content, true);

        $punchRepository = $this->createMock(PunchRepository::class);

        $punchRepository->expects($this->once())
            ->method('fetchAll')
            ->willReturn($allPunches);

        return $punchRepository;
    }
}
